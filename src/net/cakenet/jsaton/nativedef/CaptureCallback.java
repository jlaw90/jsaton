package net.cakenet.jsaton.nativedef;

public interface CaptureCallback<T extends Object> {
    void callback(T param);
}