package net.cakenet.jsaton.nativedef;

import net.cakenet.jsaton.model.SimpleImage;

import java.awt.*;
import java.util.List;

public abstract class WindowReference {
    public final int[] capture() {
        Rectangle bounds = getBounds();
        return capture(0, 0, bounds.width, bounds.height);
    }

    public final SimpleImage captureImage() {
        Rectangle bounds = getBounds();
        return captureImage(0, 0, bounds.width, bounds.height);
    }

    public abstract int[] capture(int x, int y, int width, int height);

    public final SimpleImage captureImage(int x, int y, int width, int height) {
        return new SimpleImage(capture(x, y, width, height), width, height);
    }

    public abstract String getTitle();

    public abstract Rectangle getBounds();

    public abstract List<WindowReference> children();

    public abstract WindowReference parent();

    public int getPixel(int x, int y) {
        return capture(x, y, 1, 1)[0];
    }
}