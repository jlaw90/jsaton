package net.cakenet.jsaton.model;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.font.FontRenderContext;
import java.awt.image.BufferedImage;

public class RawFont {
    private static final char[] GLYPHS;
    private final ImageMask[] glyphs = new ImageMask[255];

    public RawFont(ImageMask[] glyphs) {
        if(glyphs.length != 255)
            throw new IllegalArgumentException("Supplied glyphs are wrong length (requires 255)");
        System.arraycopy(glyphs, 0, this.glyphs, 0, 255);
    }

    public ImageMask mask(String str) {
        int height = 0;
        int width = 0;
        char[] chars = str.toCharArray();
        for (char aChar : chars) {
            ImageMask mask = glyphs[aChar];
            if (mask == null)
                continue;
            width += mask.width;
            if (mask.height > height)
                height = mask.height;
        }
        ImageMask mask = new ImageMask(width, height);
        int x = 0;
        for(int i = 0; i < chars.length; i++) {
            ImageMask m = glyphs[chars[i]];
            if(m == null)
                continue;

            int destOff = x;
            int srcOff = 0;
            for(int y = 0; y < m.height; y++) {
                System.arraycopy(m.data, srcOff, mask.data, destOff, m.width);
                srcOff += m.width;
                destOff += mask.width;
            }
            x += m.width;
        }
        return mask;
    }

    private RawFont() {
    }

    public static RawFont fromSystemFont(String name, int size, boolean bold, boolean italic) {
        int flags = Font.PLAIN;
        if(bold)
            flags |= Font.BOLD;
        if(italic)
            flags |= Font.ITALIC;
        Font f = new Font(name, flags, size);
        FontRenderContext frc = new FontRenderContext(null, false, false);
        // Get max bounds...
        int maxHeight = (int) Math.ceil(f.getLineMetrics(GLYPHS, 0, GLYPHS.length, frc).getHeight());
        int maxWidth = maxHeight * 4; // Should be fine...
        BufferedImage img = new BufferedImage(maxWidth, maxHeight, BufferedImage.TYPE_BYTE_GRAY);
        Graphics2D gfx = img.createGraphics();
        gfx.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        gfx.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_OFF);
        gfx.setFont(f);
        FontMetrics metrics = gfx.getFontMetrics();
        // Todo: glyph spacing...
        ImageMask[] glyphs = new ImageMask[255];
        int descent = metrics.getDescent();
        int ascent = metrics.getAscent();
        int height = metrics.getHeight();
        for(int i = 0; i < 255; i++) {
            char c = GLYPHS[i];
            Character.UnicodeBlock block = Character.UnicodeBlock.of( c );
            if(Character.isISOControl(c) || c == KeyEvent.CHAR_UNDEFINED || block == null || block == Character.UnicodeBlock.SPECIALS)
                continue;
            String s = String.valueOf(GLYPHS[i]);
            int width = metrics.stringWidth(s);
            gfx.setColor(Color.WHITE);
            gfx.fillRect(0, 0, maxWidth, maxHeight);
            gfx.setColor(Color.BLACK);
            gfx.drawString(s, 0, ascent);
            glyphs[i] = ImageMask.fromGrayscale(RawImage.fromImage(img.getSubimage(0, 0, width, height)));
        }
        return new RawFont(glyphs);
    }

    static {
        GLYPHS = new char[255];
        for(int i = 0; i < GLYPHS.length; i++)
            GLYPHS[i] = (char) i;
    }
}