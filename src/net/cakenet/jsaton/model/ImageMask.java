package net.cakenet.jsaton.model;

public class ImageMask {
    public final byte[] data;
    public final int width;
    public final int height;

    public ImageMask(int width, int height) {
        this.data = new byte[width * height];
        this.width = width;
        this.height = height;
    }

    public RawImage toImage() {
        int[] pixels = new int[data.length];
        for(int i = 0; i < data.length; i++) {
            int b = data[i] & 0xff;
            pixels[i] = (b << 16) | (b << 8) | b;
        }
        return new RawImage(pixels, width, height);
    }

    public static ImageMask fromGrayscale(RawImage image) {
        int w = image.width;
        int h = image.height;
        ImageMask mask = new ImageMask(w, h);
        for(int i = 0; i < image.pixels.length; i++) {
            int p = image.pixels[i];
            int r = (p >> 16) & 0xff;
            int g = (p >> 8) & 0xff;
            int b = p & 0xff;
            mask.data[i] = (byte) ((r+g+b)/3);
        }
        return mask;
    }
}