package net.cakenet.jsaton.model;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class HSLColor {
    private static Map<Integer, HSLColor> cache = Collections.synchronizedMap(new HashMap<Integer, HSLColor>());

    RGBColor rgb;
    private final int hash;
    public final int hue;
    public final int saturation;
    public final int lightness;

    private HSLColor(int h, int s, int l) {
        hue = h;
        saturation = s;
        lightness = l;
        hash = (h << 16) | (s << 8) | l;
    }

    /*public RGBColor toRGB() {
        // Todo: use cache
        // solve for C: s = l > 0.5f ? C / (2f - M - m) : C / (M + m);
        float C = saturation * (1f - Math.abs(lightness*2f-1f));
        float h = hue * 6f;
        float r, g, b;
        float X = C * (1f - Math.abs(h%2f - 1f));
        int xi = (int) Math.floor(X);
        switch(xi) {
            case 0:
                r = C;
                g = X;
                b = 0;
                break;
            case 1:
                r = X;
                g = C;
                b = 0;
                break;
            case 2:
                r = 0;
                g = C;
                b = X;
                break;
            case 3:
                r = 0;
                g = X;
                b = C;
                break;
            case 4:
                r = X;
                g = 0;
                b = C;
                break;
            case 5:
                r = C;
                g = 0;
                b = X;
                break;
            default:
                throw new RuntimeException("WHA?");
        }
        float m = lightness - (C / 2f);
        r += m;
        g += m;
        b += m;
        rgb = RGBColor.from((int) (r * 255f), (int) (g * 255f), (int) (b * 255f));
        return rgb;
    }*/

    public int hashCode() {
        return hash;
    }

    public boolean equals(Object o) {
        return o == this || (o instanceof HSLColor && equals((HSLColor) o));
    }

    public boolean equals(HSLColor o) {
        return o != null && o.hash == hash;
    }

    public static HSLColor from(float h, float s, float l) {
        int key = (((int) (h * 255)) << 16) | (((int) (s * 255)) << 8) | (((int) (l * 256)));
        if(!cache.containsKey(key))
            cache.put(key, new HSLColor((int) (h*255f), (int) (s*255f), (int) (l*255f)));
        return cache.get(key);
    }
}