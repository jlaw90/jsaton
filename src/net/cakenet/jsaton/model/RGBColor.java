package net.cakenet.jsaton.model;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class RGBColor {
    private static final Map<Integer, RGBColor> cache = Collections.synchronizedMap(new HashMap<Integer, RGBColor>());

    HSLColor hsl;
    public final int rgb;
    public final int red;
    public final int green;
    public final int blue;


    private RGBColor(int rgb) {
        this.rgb = rgb;
        red = ((rgb >> 16) & 0xff);
        green = ((rgb >> 8) & 0xff);
        blue = (rgb & 0xff);
    }

    public HSLColor toHSL() {
        if(hsl != null)
            return hsl;
        float r = (float) this.red / 255f;
        float g = (float) this.green / 255f;
        float b = (float) this.blue / 255f;
        float M = Math.max(Math.max(r, g), b);
        float m = Math.min(Math.min(r, g), b);
        float h, s;
        float l = (M + m) / 2f; // lightness
        float C = M - m;
        if(C == 0) {
            h = s = 0; // grayscale
        } else {
            s = C / (1f - Math.abs(l*2-1f));
            if(M == r)
                h = ((g - b) / C) % 6f;
            else if(M == g)
                h = (b - r) / C + 2f;
            else if(M == b)
                h = (r - g) / C + 4f;
            else throw new RuntimeException("WHAT?");
        }
        h /= 6f;
        hsl = HSLColor.from(h, s, l);
        hsl.rgb = this;
        return hsl;
    }

    public int hashCode() {
        return rgb;
    }

    public boolean equals(Object o) {
        return o == this || (o instanceof RGBColor && equals((RGBColor) o));
    }

    public boolean equals(RGBColor o) {
        return o != null && o.rgb == rgb;
    }

    public static RGBColor from(int rgb) {
        rgb &= 0xffffff; // Exclude alpha
        if (!cache.containsKey(rgb))
            cache.put(rgb, new RGBColor(rgb));
        return cache.get(rgb);
    }

    public static RGBColor from(int r, int g, int b) {
        return from((r << 16) | (g << 8) | b);
    }
}