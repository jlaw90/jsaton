package net.cakenet.jsaton.script;

public enum ScriptLanguage {
    RUBY("ruby", "rbg");

    public final String name, extension;

    ScriptLanguage(String name, String ext) {
        this.name = name;
        this.extension = ext;
    }
}