package net.cakenet.jsaton.script.debug;

import java.util.*;

public class BreakInformation {
    public List<DebugFrame> frames = new Stack<>();
    public Map<Class, VariableExtractor> variableConverters = new HashMap<>();

    public BreakInformation() {
        variableConverters.put(Object.class, JavaExtractor.instance);
    }

    public DebugFrame pushFrame(String name, String source, int line, Object context, Map<String, Object> vars) {
        DebugFrame f = new DebugFrame();
        List<DebugObject> converted = new LinkedList<>();
        for(String key: vars.keySet()) {
            DebugObject convert = convert(vars.get(key));
            if(convert == null)
                convert = new SimpleDebugObject();
            convert.name = key;
            converted.add(convert);
        }
        f.name = name;
        f.line = line;
        f.source = source;
        f.self = convert(context);
        f.variables = converted;
        frames.add(f);
        return f;
    }

    private DebugObject convert(Object o) {
        if(o == null)
            return null;
        Class c = o.getClass();
        outer:
        while(!variableConverters.containsKey(c) && c != null) {
            for(Class iface: c.getInterfaces()) {
                if(variableConverters.containsKey(iface)) {
                    c = iface;
                    break outer;
                }
            }
            c = c.getSuperclass();
        }
        VariableExtractor vc = variableConverters.get(c);
        if(vc == null)
            throw new RuntimeException("No variable converter for type: " + o.getClass().getName());
        return vc.extractFrom(o);
    }

    public <T> void addVariableConverter(Class<? extends T> clazz, VariableExtractor<T> provider) {
        variableConverters.put(clazz, provider);
    }
}