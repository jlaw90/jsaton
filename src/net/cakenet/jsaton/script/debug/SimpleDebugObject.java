package net.cakenet.jsaton.script.debug;

import java.util.List;

public class SimpleDebugObject extends DebugObject {
    public List<DebugObject> variables;

    public SimpleDebugObject() {
        this(null, null, null);
    }

    public SimpleDebugObject(String name, String type, Object value) {
        this.name = name;
        this.type = type;
        this.value = value;
    }

    public int indexOf(Object o) {
        return variables.indexOf(o);
    }

    public int size() {
        return variables == null? 0: variables.size();
    }

    public DebugObject get(int index) {
        return variables.get(index);
    }
}