package net.cakenet.jsaton.script.debug;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;

public class Breakpoint {
    public static final BreakpointAction ACTION_LOG = new BreakpointAction() {
        public void execute(Breakpoint source, BreakInformation info) {
            System.out.println("Breakpoint hit");
        }
    };
    public static final BreakpointAction ACTION_ACCUMULATE = new BreakpointAction() {
        public void execute(Breakpoint source, BreakInformation info) {
            source.accumulator++;
        }
    };

    public BreakType type;
    public List<BreakpointAction> actions = new LinkedList<>();
    public List<Breakpoint> dependencies = new LinkedList<>();
    private int accumulator;
    public int passCount;
    private String condition;

    public Breakpoint(BreakType type) {
        this.type = type;
    }

    public boolean shouldSuspend() {
        if(this.type != BreakType.SUSPEND)
            return false;
        return true; // Todo: loads of shit still to do!
    }

    public void doActions(BreakInformation info) {
        for(BreakpointAction action: actions)
            action.execute(this, info);
    }

    public void save(OutputStream os) throws IOException {
        os.write(type.ordinal());

        // Todo: lots more to be done here!
    }

    public static Breakpoint read(InputStream in) throws IOException {
        Breakpoint b = new Breakpoint(BreakType.values()[in.read() & 0xff]);
        // Todo: same as in save...
        return b;
    }
}