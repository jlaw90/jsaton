package net.cakenet.jsaton.script.debug;

public abstract class BreakpointAction {
    public abstract void execute(Breakpoint point, BreakInformation info);
}