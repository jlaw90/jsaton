package net.cakenet.jsaton.script.debug;

public class OnDemandDebugObject extends DebugObject {
    private DebugObjectProvider provider;
    private DebugObject provided;

    public OnDemandDebugObject(DebugObjectProvider provider) {
        this(provider, null, null,  null);
    }

    public OnDemandDebugObject(DebugObjectProvider provider, String name, String type, Object value) {
        this.provider = provider;
        this.name = name;
        this.type = type;
        this.value = value;
    }

    private void ensureProvided() {
        if(provided != null)
            return;
        provided = provider.provide();
        if(provided == null)
            throw new RuntimeException("Provided nothing!");
        provided.setName(name);
    }

    public int size() {
        ensureProvided();
        return provided.size();
    }

    public DebugObject get(int index) {
        ensureProvided();
        return provided.get(index);
    }

    public int indexOf(Object obj) {
        ensureProvided();
        return provided.indexOf(obj);
    }

    public String getType() {
        ensureProvided();
        return provided.getType();
    }

    public Object getValue() {
        ensureProvided();
        return provided.getValue();
    }

    public String toString() {
        ensureProvided();
        return provided.toString();
    }
}