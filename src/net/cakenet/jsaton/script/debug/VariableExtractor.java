package net.cakenet.jsaton.script.debug;

public interface VariableExtractor<T> {
    public DebugObject extractFrom(Object o);
}