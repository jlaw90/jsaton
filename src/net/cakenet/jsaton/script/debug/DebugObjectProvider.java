package net.cakenet.jsaton.script.debug;

public interface DebugObjectProvider {
    DebugObject provide();
}