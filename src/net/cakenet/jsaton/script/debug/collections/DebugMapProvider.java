package net.cakenet.jsaton.script.debug.collections;

public interface DebugMapProvider {
    int size();

    DebugMapElement get(int index);

    int indexOf(Object child);
}