package net.cakenet.jsaton.script.debug.collections;

import net.cakenet.jsaton.script.debug.DebugObject;

public class DebugArrayElement extends DebugObject {
    public int index;
    public DebugObject value;

    public DebugArrayElement(int index, DebugObject value) {
        this.value = value;
        this.index = index;
    }

    public String getType() {
        return value.getType();
    }

    public Object getValue() {
        return value.getValue();
    }

    public int size() {
        return value.size();
    }

    public int indexOf(Object child) {
        return value.indexOf(child);
    }

    public DebugObject get(int index) {
        return value.get(index);
    }

    public String toString() {
        return "[" + String.valueOf(index) + "] = " + value.toString();
    }
}