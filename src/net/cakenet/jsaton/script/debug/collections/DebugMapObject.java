package net.cakenet.jsaton.script.debug.collections;

import net.cakenet.jsaton.script.debug.DebugObject;

public class DebugMapObject extends DebugObject {
    private DebugMapProvider provider;

    public DebugMapObject(DebugMapProvider provider) {
        this.provider = provider;
    }

    public int size() {
        return provider.size();
    }

    public int indexOf(Object child) {
        return provider.indexOf(child);
    }

    public DebugObject get(int index) {
        return provider.get(index);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        String name = getName();
        if(name != null)
            sb.append(name).append(" ");
        return sb.append("{...}").toString();
    }
}