package net.cakenet.jsaton.script.debug.collections;

public interface DebugArrayProvider {
    int size();

    DebugArrayElement get(int index);

    int indexOf(Object child);
}