package net.cakenet.jsaton.script.debug.collections;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractDebugMapProvider implements DebugMapProvider {
    private List<DebugMapElement> provided = new ArrayList<>();
    private int index;

    protected AbstractDebugMapProvider() {
    }

    protected abstract DebugMapElement next();

    public DebugMapElement get(int index) {
        final int size = index+1;
        while(this.index < size)
            provideNext();
        return provided.get(index);
    }

    private void provideNext() {
        provided.add(next());
        index++;
    }

    public int indexOf(Object o) {
        return provided.indexOf(o);
    }
}