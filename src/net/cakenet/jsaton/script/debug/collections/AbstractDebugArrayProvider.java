package net.cakenet.jsaton.script.debug.collections;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractDebugArrayProvider implements DebugArrayProvider {
    private List<DebugArrayElement> provided = new ArrayList<>();
    protected int index;

    public DebugArrayElement get(int index) {
        final int size = index + 1;
        while(this.index < size) {
            DebugArrayElement next = next();
            next.index = this.index++;
            provided.add(next);
        }

        return provided.get(index);
    }

    protected abstract DebugArrayElement next();

    public int indexOf(Object child) {
        return provided.indexOf(child);
    }
}