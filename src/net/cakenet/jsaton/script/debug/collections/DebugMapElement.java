package net.cakenet.jsaton.script.debug.collections;

import net.cakenet.jsaton.script.debug.DebugObject;

public class DebugMapElement extends DebugObject {
    public DebugObject key;
    public DebugObject value;

    public DebugMapElement(DebugObject key, DebugObject value) {
        this.key = key;
        this.value = value;
        key.setName("key");
        value.setName("value");
    }

    public int size() {
        return 2;
    }

    public int indexOf(Object child) {
        return key == child? 0: 1;
    }

    public DebugObject get(int index) {
        if(index == 0)
            return key;
        return value;
    }


    public String toString() {
        return toString(key.getValue()) + " => " + toString(value);
    }
}