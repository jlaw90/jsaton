package net.cakenet.jsaton.script.debug;

public abstract class DebugObject {
    protected DebugObject parent;
    protected String name, type;
    protected Object value;

    protected DebugObject() {}

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public Object getValue() {
        return value;
    }

    public abstract int size();

    public abstract int indexOf(Object child);

    public abstract DebugObject get(int index);

    public String toString() {
        StringBuilder sb = new StringBuilder();
        String name = getName();
        Object value = getValue();
        if(name != null) {
            sb.append(name);
            if(value!=null)
                sb.append(" = ");
        }
        if(value != null)
            sb.append(toString(value));
        return sb.toString();
    }

    public static String toString(Object o) {
        if(o == null)
            return "null";
        if(o instanceof String)
            return "\"" + ((String) o).replace("\"", "\\\"") + "\"";
        if(o instanceof DebugObject)
            return toString(((DebugObject) o).getValue());
        try {
           return o.toString();
        } catch(Throwable t) {
            return t.toString();
        }
    }
}