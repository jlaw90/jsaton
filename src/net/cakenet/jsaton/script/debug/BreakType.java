package net.cakenet.jsaton.script.debug;

public enum BreakType {
    SUSPEND,
    CONTINUE
}