package net.cakenet.jsaton.script.debug;

import net.cakenet.jsaton.script.Script;

public interface DebugBreakListener {
    public void onBreak(Script source, BreakInformation info);
}