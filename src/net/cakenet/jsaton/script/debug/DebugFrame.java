package net.cakenet.jsaton.script.debug;

import java.util.List;

public class DebugFrame {
    public String name;
    public String source;
    public int line;
    public DebugObject self;
    public List<DebugObject> variables;
}