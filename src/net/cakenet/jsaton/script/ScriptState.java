package net.cakenet.jsaton.script;

public enum ScriptState {
    STOPPED,
    SUSPENDED,
    RUNNING,
    INITIALISING
}