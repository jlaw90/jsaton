package net.cakenet.jsaton.script;

import java.io.FileDescriptor;
import java.lang.reflect.Member;
import java.net.InetAddress;
import java.security.Permission;

public class ScriptSecurityManager extends SecurityManager {
    public static final ThreadGroup SecureThreadGroup = new ThreadGroup("secure.threads");

    public void checkSecurityAccess(String target) {
        if (safeThread())
            return;
        if(!checkAccess("security " + target))
            throw new SecurityException();
    }

    public void checkMemberAccess(Class<?> clazz, int which) {
        if (safeThread())
            return;
        if(!checkAccess((which == Member.DECLARED? "declared": " public") + " class '" + clazz.getName() + "'"))
            throw new SecurityException();
    }

    public void checkSetFactory() {
        if (safeThread())
            return;
        if(!check("set the socket factory used by ServerSocket or Socket, or the stream handler factory used by URL"))
            throw new SecurityException();
    }

    public void checkPackageDefinition(String pkg) {
        if (safeThread())
            return;
        if(!check("define package " + pkg))
            throw new SecurityException();
    }

    public void checkPackageAccess(String pkg) {
        // Allow all package access for now...
        // Todo: should probably whitelist stuff
    }

    public void checkAwtEventQueueAccess() {
        if (safeThread())
            return;
        if(!checkAccess("the AWT event queue"))
            throw new SecurityException();
    }

    public void checkSystemClipboardAccess() {
        if (safeThread())
            return;
        if(!checkAccess("the clipboard"))
            throw new SecurityException();
    }

    public boolean checkTopLevelWindow(Object window) {
        if (safeThread())
            return true;
        return check("create a top level window");
    }

    public void checkPrintJobAccess() {
        if (safeThread())
            return;
        if(!checkAccess("printing support"))
            throw new SecurityException();
    }

    public void checkPropertyAccess(String key) {
        if (safeThread())
            return;
        if(!checkAccess("system property " + key))
            throw new SecurityException();
    }

    public void checkPropertiesAccess() {
        if (safeThread())
            return;
        if(!checkAccess("system properties"))
            throw new SecurityException();
    }

    public void checkMulticast(InetAddress maddr) {
        if (safeThread())
            return;
        if(!check("multicast " + maddr))
            throw new SecurityException();
    }

    public void checkAccept(String host, int port) {
        if (safeThread())
            return;
        if(!check("accept connection on port " + port + " from " + host))
            throw new SecurityException();
    }

    public void checkListen(int port) {
        if (safeThread())
            return;
        if(!check("listen for connections on port " + port))
            throw new SecurityException();
    }

    public void checkConnect(String host, int port, Object context) {
        if (safeThread())
            return;
        if(!check("connect to " + host + ":" + port + " in context of: " + context))
            throw new SecurityException();
    }

    public void checkConnect(String host, int port) {
        if (safeThread())
            return;
        if(!check("connect to " + host + ":" + port))
            throw new SecurityException();
    }

    public void checkDelete(String file) {
        if (safeThread())
            return;
        if(!check("delete file " + file))
            throw new SecurityException();
    }

    public void checkWrite(String file) {
        if (safeThread())
            return;
        if(!check("write file " + file))
            throw new SecurityException();
    }

    public void checkWrite(FileDescriptor fd) {
        if (safeThread())
            return;
        if(!check("write file descriptor " + fd))
            throw new SecurityException();
    }

    public void checkRead(String file, Object context) {
        if (safeThread())
            return;
        if(!check("read file '" + file + "' in context of " + context))
            throw new SecurityException();
    }

    public void checkRead(String file) {
        if (file.endsWith(".class") || safeThread()) // don't block class loading...
            return;
        if(!check("read file " + file))
            throw new SecurityException();
    }

    public void checkRead(FileDescriptor fd) {
        if (safeThread())
            return;
        if(!check("read file descriptor: " + fd))
            throw new SecurityException();
    }

    public void checkLink(String lib) {
        if (safeThread())
            return;
        if(!check("dynamic link " + lib))
            throw new SecurityException();
    }

    public void checkExec(String cmd) {
        if (safeThread())
            return;
        if(!check("execute '" + cmd + "'"))
            throw new SecurityException();
    }

    public void checkExit(int status) {
        if (safeThread())
            return;
        if(!check("exit the application with status " + status))
            throw new SecurityException();
    }

    public void checkAccess(ThreadGroup g) {
        if (g == SecureThreadGroup || safeThread())
            return;
        if(!checkAccess("threadgroup " + g.getName()))
            throw new SecurityException();
    }

    public void checkCreateClassLoader() {
        if (safeThread())
            return;
        if(!check("create a class loader"))
            throw new SecurityException();
    }

    public void checkAccess(Thread t) {
        if (safeThread())
            return;
        if(!checkAccess("thread " + t.getName()))
            throw new SecurityException();
    }

    public void checkPermission(Permission perm, Object context) {
        if (safeThread())
            return;
        if(!checkAccess("permission " + perm.getName() + " with context: " + context))
            throw new SecurityException();
    }

    public void checkPermission(Permission perm) {
        if (safeThread())
            return;
        if(!checkAccess("permission " + perm.getName()))
            throw new SecurityException();
    }

    public ThreadGroup getThreadGroup() {
        if (safeThread())
            return super.getThreadGroup();
        return SecureThreadGroup;
    }

    private static boolean safeThread() {
        return !SecureThreadGroup.parentOf(Thread.currentThread().getThreadGroup());
    }

    private boolean checkAccess(String permission) {
        return check("access " + permission);
    }

    private boolean check(String accessStr) {
        //System.out.println(accessStr);
        return true; // Todo: actual security
    }
}