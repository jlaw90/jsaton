package net.cakenet.jsaton;

import net.cakenet.jsaton.script.ScriptSecurityManager;
import net.cakenet.jsaton.ui.MainWindow;

import javax.swing.*;
import java.awt.*;

public class Main {
    public static final String APP_NAME = "JSATON";
    public static final String APP_VER = "0.1a";

    public static void main(String[] args) {
        System.setSecurityManager(new ScriptSecurityManager());
        JFrame.setDefaultLookAndFeelDecorated(true);
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        JFrame jf = new MainWindow();
        jf.setPreferredSize(new Dimension(800, 600));
        jf.pack();
        jf.setVisible(true);
    }
}