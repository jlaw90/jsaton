package net.cakenet.jsaton.clipboard;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ColorTransferable implements Transferable {
    public static final DataFlavor ColorFlavor = new DataFlavor(Color.class, "application/x-jsaton-color");
    private static final List<DataFlavor> supported = Arrays.asList(DataFlavor.stringFlavor, ColorFlavor);

    public final Color color;

    public ColorTransferable(Color color) {
        this.color = color;
    }

    public DataFlavor[] getTransferDataFlavors() {
        return supported.toArray(new DataFlavor[supported.size()]);
    }

    public boolean isDataFlavorSupported(DataFlavor flavor) {
        return supported.contains(flavor);
    }

    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
        if(flavor.equals(ColorFlavor))
            return color;
        else if(flavor.equals(DataFlavor.stringFlavor))
            return String.format("0x%06x", color.getRGB() & 0xffffff);
        else
            throw new UnsupportedFlavorException(flavor);
    }
}