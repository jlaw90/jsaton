package net.cakenet.jsaton.clipboard;

import net.cakenet.jsaton.model.RawImage;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class SimpleImageTransferable implements Transferable {
    public static final DataFlavor SimpleImageFlavor = new DataFlavor(RawImage.class, "application/x-jsaton-simpleimage");;
    private static final List<DataFlavor> supported = Arrays.asList(DataFlavor.imageFlavor, DataFlavor.stringFlavor, SimpleImageFlavor);

    public final RawImage image;

    public SimpleImageTransferable(RawImage image) {
        this.image = image;
    }

    public DataFlavor[] getTransferDataFlavors() {
        return supported.toArray(new DataFlavor[supported.size()]);
    }

    public boolean isDataFlavorSupported(DataFlavor flavor) {
        return supported.contains(flavor);
    }

    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
        if(flavor.equals(SimpleImageFlavor)) {
            return image;
        } else if(flavor.equals(DataFlavor.imageFlavor))
            return image.toImage();
        else if(flavor.equals(DataFlavor.stringFlavor))
            return image.encode();
        else
            throw new UnsupportedFlavorException(flavor);
    }
}