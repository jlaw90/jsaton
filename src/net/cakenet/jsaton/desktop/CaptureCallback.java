package net.cakenet.jsaton.desktop;

public interface CaptureCallback<T extends Object> {
    void callback(T param);
}