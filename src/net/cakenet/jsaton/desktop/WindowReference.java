package net.cakenet.jsaton.desktop;

import net.cakenet.jsaton.model.RawImage;

import java.awt.*;
import java.util.List;

public abstract class WindowReference {
    public final int[] capture() {
        Rectangle bounds = getBounds();
        return capture(0, 0, bounds.width, bounds.height);
    }

    public final RawImage captureImage() {
        Rectangle bounds = getBounds();
        return captureImage(0, 0, bounds.width, bounds.height);
    }

    public abstract int[] capture(int x, int y, int width, int height);

    public final RawImage captureImage(int x, int y, int width, int height) {
        return new RawImage(capture(x, y, width, height), width, height);
    }

    public abstract String getTitle();

    public abstract Rectangle getBounds();

    public final int getWidth() {
        return getBounds().width;
    }

    public final int getHeight() {
        return getBounds().height;
    }

    public abstract List<WindowReference> children();

    public abstract WindowReference parent();

    public int getPixel(int x, int y) {
        return capture(x, y, 1, 1)[0];
    }
}