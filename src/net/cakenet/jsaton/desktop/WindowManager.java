package net.cakenet.jsaton.desktop;

import java.awt.*;
import java.util.List;
import java.util.Stack;

public abstract class WindowManager {
    private static final WindowManager instance = create();
    protected static Robot robot;

    protected abstract WindowReference findWindowByPointImpl(Point loc);

    protected abstract WindowReference getDesktopImpl();

    protected abstract List<WindowReference> getTopLevelWindowsImpl();

    private static WindowManager create() {
        String os = System.getProperty("os.name").toLowerCase();
        if (os.startsWith("windows"))
            return new WindowsWindowManager();
        else if (os.startsWith("mac"))
            System.out.println("Will attempt to us X11 windowing capture.  Currently untested on mac!");
        return new X11WindowManager();
    }

    public static WindowReference findWindowByPoint(Point p) {
        return instance.findWindowByPointImpl(p);
    }

    public static WindowReference getDesktop() {
        return instance.getDesktopImpl();
    }

    public static List<WindowReference> getTopLevelWindows() {
        return instance.getTopLevelWindowsImpl();
    }

    public static WindowReference findWindowByName(String name) {
        Stack<WindowReference> stack = new Stack<>();
        stack.add(getDesktop());
        while(!stack.isEmpty()) {
            WindowReference w = stack.pop();
            if(name.equalsIgnoreCase(w.getTitle()))
                return w;
            stack.addAll(w.children());
        }
        return null;
    }

    static {
        try {
            robot = new Robot();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}