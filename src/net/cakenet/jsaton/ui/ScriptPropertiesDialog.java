/*
 * Created by JFormDesigner on Mon Feb 11 14:55:45 GMT 2013
 */

package net.cakenet.jsaton.ui;

import net.cakenet.jsaton.script.Script;
import net.cakenet.jsaton.script.ScriptLanguage;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author James Lawrence
 */
public class ScriptPropertiesDialog extends JDialog {
    private Script source;
    private boolean selected = false;

    public ScriptPropertiesDialog(Frame owner) {
        super(owner, "New script");
        initComponents();
        language.setModel(new DefaultComboBoxModel<>(ScriptLanguage.values()));
    }

    public ScriptPropertiesDialog(Frame owner, Script source) {
        this(owner);
        setTitle("Script properties");
        language.setSelectedItem(source.language);
        language.setEnabled(false);
        game.setText(source.getGame());
        name.setText(source.getName());
        author.setText(source.getAuthor());
        description.setText(source.getDescription());
        this.source = source;
    }

    public Script getResult() {
        if(!selected)
            return null;
        Script s = source == null? Script.create((ScriptLanguage) language.getSelectedItem()): source;
        s.setName(name.getText());
        s.setAuthor(author.getText());
        s.setGame(game.getText());
        s.setDescription(description.getText());
        return s;
    }

    private void okButtonActionPerformed(ActionEvent e) {
        selected = true;
        setVisible(false);
    }

    private void cancelButtonActionPerformed(ActionEvent e) {
        setVisible(false);
    }

    private void languageActionPerformed(ActionEvent e) {

    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        label1 = new JLabel();
        language = new JComboBox();
        label3 = new JLabel();
        name = new JTextField();
        label6 = new JLabel();
        author = new JTextField();
        label9 = new JLabel();
        game = new JTextField();
        descLabel = new JLabel();
        scrollPane1 = new JScrollPane();
        description = new JTextPane();
        buttonBar = new JPanel();
        okButton = new JButton();
        cancelButton = new JButton();

        //======== this ========
        setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        setResizable(false);
        setModal(true);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setType(Window.Type.POPUP);
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {
                contentPanel.setLayout(new GridBagLayout());

                //---- label1 ----
                label1.setText("Language");
                label1.setHorizontalAlignment(SwingConstants.TRAILING);
                label1.setLabelFor(language);
                contentPanel.add(label1, new GridBagConstraints(0, 0, 1, 1, 0.4, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- language ----
                language.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        languageActionPerformed(e);
                    }
                });
                contentPanel.add(language, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- label3 ----
                label3.setText("Name");
                label3.setHorizontalAlignment(SwingConstants.TRAILING);
                contentPanel.add(label3, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                contentPanel.add(name, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- label6 ----
                label6.setText("Author");
                label6.setHorizontalAlignment(SwingConstants.TRAILING);
                contentPanel.add(label6, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                contentPanel.add(author, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- label9 ----
                label9.setText("Game");
                label9.setHorizontalAlignment(SwingConstants.TRAILING);
                contentPanel.add(label9, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                contentPanel.add(game, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- descLabel ----
                descLabel.setText("Description");
                descLabel.setHorizontalAlignment(SwingConstants.TRAILING);
                contentPanel.add(descLabel, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));

                //======== scrollPane1 ========
                {

                    //---- description ----
                    description.setMinimumSize(new Dimension(50, 60));
                    description.setPreferredSize(new Dimension(250, 70));
                    scrollPane1.setViewportView(description);
                }
                contentPanel.add(scrollPane1, new GridBagConstraints(1, 4, 1, 1, 0.0, 1.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 85, 80};
                ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0};

                //---- okButton ----
                okButton.setText("OK");
                okButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        okButtonActionPerformed(e);
                    }
                });
                buttonBar.add(okButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));

                //---- cancelButton ----
                cancelButton.setText("Cancel");
                cancelButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        cancelButtonActionPerformed(e);
                    }
                });
                buttonBar.add(cancelButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.PAGE_END);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JLabel label1;
    private JComboBox language;
    private JLabel label3;
    private JTextField name;
    private JLabel label6;
    private JTextField author;
    private JLabel label9;
    private JTextField game;
    private JLabel descLabel;
    private JScrollPane scrollPane1;
    private JTextPane description;
    private JPanel buttonBar;
    private JButton okButton;
    private JButton cancelButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
