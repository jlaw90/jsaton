/*
 * Created by JFormDesigner on Wed Feb 13 16:00:49 GMT 2013
 */

package net.cakenet.jsaton.ui;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * @author James Lawrence
 */
public class AboutDialog extends JDialog {
    public AboutDialog(Frame owner) {
        super(owner);
        initComponents();
    }

    public AboutDialog(Dialog owner) {
        super(owner);
        initComponents();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        panel1 = new JScrollPane();
        textArea1 = new JTextPane();
        buttonBar = new JPanel();
        okButton = new JButton();

        //======== this ========
        setTitle("About");
        setModal(true);
        setResizable(false);
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {
                contentPanel.setLayout(new BorderLayout());

                //======== panel1 ========
                {

                    //---- textArea1 ----
                    textArea1.setText("<html><body>\nFollowing is a list of resources that we use and corresponding licenses:<br/>\n<a href=\"http://fifesoft.com/rsyntaxtextarea/\">RSyntaxTextArea</a> <small>(<a href=\"http://fifesoft.com/rsyntaxtextarea/RSyntaxTextArea.License.txt\">License</a>)</small><br/>\n<a href=\"http://jruby.org\">JRuby</a> <small>(<a href=\"https://github.com/jruby/jruby/blob/master/LICENSE.RUBY\">License</a>)</small><br/>\n<a href=\"https://github.com/twall/jna\">JNA</a> <small>(<a href=\"https://github.com/twall/jna/blob/master/LICENSE\">License</a>)</small><br/>\n<a href=\"http://forge.scilab.org/index.php/p/flexdock/\">FlexDock</a> <small>(<a href=\"http://forge.scilab.org/index.php/p/flexdock/source/tree/master/LICENSE.txt\">License</a>)</small>\n</body></html>");
                    textArea1.setContentType("text/html");
                    panel1.setViewportView(textArea1);
                }
                contentPanel.add(panel1, BorderLayout.CENTER);
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 80};
                ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0};

                //---- okButton ----
                okButton.setText("OK");
                buttonBar.add(okButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        setSize(655, 415);
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JScrollPane panel1;
    private JTextPane textArea1;
    private JPanel buttonBar;
    private JButton okButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
