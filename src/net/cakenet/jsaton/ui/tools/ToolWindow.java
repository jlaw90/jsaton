package net.cakenet.jsaton.ui.tools;

import org.flexdock.docking.DockingConstants;
import org.flexdock.docking.event.DockingListener;
import org.flexdock.plaf.PlafManager;
import org.flexdock.view.Titlebar;
import org.flexdock.view.View;

import javax.swing.*;
import java.awt.*;

public abstract class ToolWindow extends View implements DockingListener {
    public ToolWindow(String title) {
        super(title);
        getContentPane().setLayout(new BorderLayout());
        setTitle(title);
        Titlebar bar = getTitlebar();
        bar.setText(title);
        Icon i = getIcon();
        if (i != null) {
            getDockingProperties().setDockbarIcon(i);
            getDockingProperties().setTabIcon(i);
            bar.setIcon(i);
        }
        if (isCloseable())
            addAction(DockingConstants.CLOSE_ACTION);
        if (isPinnable())
            addAction(DockingConstants.PIN_ACTION);
        if (!hasTitlebar())
            setTitlebar(null);
        setName(title);
    }

    public String getTitle() {
        return getDockingProperties().getDockableDesc();
    }

    public void setTitle(String title) {
        getDockingProperties().setDockableDesc(title);
    }

    public abstract Component getContents();

    public Icon getIcon() {
        return null;
    }

    public boolean isPinnable() {
        return true;
    }

    public boolean isCloseable() {
        return true;
    }

    public boolean hasTitlebar() {
        return true;
    }

    static {
        PlafManager.setPreferredTheme("win32");
    }
}