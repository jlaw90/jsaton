package net.cakenet.jsaton.ui.tools.snippet;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.lang.ref.WeakReference;
import java.util.LinkedList;
import java.util.List;

public class SnippetComposite extends JPanel implements Scrollable {
    private static SnippetComposite currentComposite, lastComposite;
    private static volatile boolean valueChanging;
    private static final List<WeakReference<SnippetComposite>> references = new LinkedList<>();
    private JComponent idle;
    private JComponent hover;
    private JWindow tooltipWindow;
    private String name;

    public SnippetComposite(String name, JComponent id, JComponent h) {
        setLayout(new BorderLayout());
        this.name = name;
        this.idle = id;
        this.hover = h;

        // Add header...
        JPanel top = new JPanel(new BorderLayout());
        top.setOpaque(true);
        top.setBackground(new Color(0x0a, 0x24, 0x6a));
        JLabel title = new JLabel(name);
        float fontSize = title.getFont().getSize();
        title.setFont(title.getFont().deriveFont(fontSize + 1));
        title.setForeground(Color.WHITE);
        top.add(title, BorderLayout.CENTER);
        final String label = "remove";
        final String underlined = "<html><u>" + label + "</u></html>";
        final JLabel remove = new JLabel(label);
        remove.setFont(remove.getFont().deriveFont(fontSize - 1));
        remove.setForeground(Color.WHITE);
        remove.setVerticalAlignment(JLabel.CENTER);
        remove.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        remove.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent e) {
                SnippetManager.instance.remove(SnippetComposite.this);
            }

            public void mousePressed(MouseEvent e) {
            }

            public void mouseReleased(MouseEvent e) {
            }

            public void mouseEntered(MouseEvent e) {
                remove.setText(underlined);
            }

            public void mouseExited(MouseEvent e) {
                remove.setText(label);
            }
        });
        top.add(remove, BorderLayout.EAST);
        add(top, BorderLayout.NORTH);

        setBorder(BorderFactory.createLineBorder(getBackground().darker()));
        add(idle, BorderLayout.CENTER);
        idle.addMouseListener(new MouseAdapter() {
            public void mouseEntered(MouseEvent e) {
                if (tooltipWindow != null)
                    return;
                for (int i = 0; i < references.size(); i++) {
                    SnippetComposite sc = references.get(i).get();
                    if(sc == null || sc.tooltipWindow == null)
                        continue;
                    JWindow tip = sc.tooltipWindow;
                    tip.setVisible(false);
                    tip.dispose();
                    sc.tooltipWindow = null;
                }
                final JWindow window = new JWindow();
                window.getContentPane().add(hover);
                ((JComponent) window.getContentPane()).setBorder(BorderFactory.createLineBorder(Color.BLACK));
                window.setOpacity(1f);
                window.pack();
                window.setLocation(e.getXOnScreen()+20, e.getYOnScreen() - window.getHeight()/2);
                tooltipWindow = window;
                window.setVisible(true);
                window.setAlwaysOnTop(true);
            }
        });
        references.add(new WeakReference<>(this));
    }

    public Dimension getPreferredScrollableViewportSize() {
        return getSize();
    }

    public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
        return 0;
    }

    public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
        return 0;
    }

    public boolean getScrollableTracksViewportWidth() {
        return false;
    }

    public boolean getScrollableTracksViewportHeight() {
        return true;
    }

    private static boolean isDescendantOf(Component parent, Component child) {
        while (child != null) {
            if (parent == child)
                return true;
            if (child instanceof JPopupMenu) {
                child = ((JPopupMenu) child).getInvoker();
            }
            child = child.getParent();
        }
        return false;
    }

    private static SnippetComposite getComposite(Component comp) {
        for (int i = 0; i < references.size(); i++) {
            WeakReference<SnippetComposite> ref = references.get(i);
            SnippetComposite val = ref.get();
            if (val == null) {
                references.remove(ref);
                i--;
                continue;
            }
            if (val.tooltipWindow == null)
                continue;
            if (isDescendantOf(val.tooltipWindow, comp) || isDescendantOf(val.idle, comp))
                return val;
        }
        return null;
    }

    static {
        Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {
            private int timeout = 0;

            {
                Timer tooltipHider = new Timer(100, new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        SnippetComposite tip = lastComposite;
                        boolean noChange = currentComposite != null && currentComposite == lastComposite;
                        if (valueChanging || noChange || tip == null)
                            return;


                        timeout++;
                        boolean remove = timeout >= 10 || currentComposite != null;
                        if (!remove)
                            return;
                        if(tip.tooltipWindow != null) {
                        tip.tooltipWindow.setVisible(false);
                        tip.tooltipWindow.dispose();
                        tip.tooltipWindow = null;
                        }
                        lastComposite = null;
                        timeout = 0;
                    }
                });
                tooltipHider.setRepeats(true);
                tooltipHider.start();
            }

            public void eventDispatched(AWTEvent event) {
                int type = event.getID();
                MouseEvent me = (MouseEvent) event;
                Component source = me.getComponent();
                SnippetComposite compositeSrc = getComposite(source);
                SnippetComposite current = currentComposite;
                switch(type) {
                    case MouseEvent.MOUSE_MOVED:
                        if (compositeSrc != null) {
                            timeout = 0;
                            if (current != compositeSrc) {
                                valueChanging = true;
                                lastComposite = currentComposite;
                                currentComposite = compositeSrc;
                            }
                        } else if(currentComposite != null) {
                            valueChanging = true;
                            lastComposite = currentComposite;
                            currentComposite = null;
                        }
                        valueChanging = false;
                        break;
                    case MouseEvent.MOUSE_PRESSED:
                        if(compositeSrc == null || compositeSrc != currentComposite)
                            timeout += 10;
                        break;
                }
            }
        }, MouseEvent.MOUSE_MOTION_EVENT_MASK | MouseEvent.MOUSE_EVENT_MASK);
    }
}