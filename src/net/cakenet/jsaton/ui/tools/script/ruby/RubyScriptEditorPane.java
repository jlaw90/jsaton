package net.cakenet.jsaton.ui.tools.script.ruby;

import net.cakenet.jsaton.script.ruby.RubyScript;
import net.cakenet.jsaton.ui.tools.script.ScriptEditor;
import net.cakenet.jsaton.ui.tools.script.ScriptEditorPane;
import org.jruby.ast.Node;

import javax.swing.event.DocumentListener;

public class RubyScriptEditorPane extends ScriptEditorPane implements DocumentListener {
    public RubyScriptEditorPane(ScriptEditor editor) {
        super(editor);
        if(!(script instanceof RubyScript))
            throw new RuntimeException("We can only do ruby scripts...");
        getDocument().addDocumentListener(this);
        RubyScript rs = (RubyScript) script;
        super.addParser(rs);
    }

    private boolean isBreakable(Node root, int line) {
        int nl = root.getPosition().getStartLine();
        if(nl == line && ((RubyScript) script).isBreakpointTarget(root))
            return true;
        for(Node child: root.childNodes())
            if(isBreakable(child, line))
                return true;
        return false;
    }

    public boolean isBreakableLine(int line) {
        Node ast = ((RubyScript) script).getAST();
        if(ast == null)
            return true; // Will be re-evaluated later...
        return ast != null && isBreakable(ast, line);
    }
}