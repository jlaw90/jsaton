package net.cakenet.jsaton.ui.tools.script;

public interface BreakpointModificationListener {
    void breakpointAdded(int line);

    void breakpointRemoved(int line);
}