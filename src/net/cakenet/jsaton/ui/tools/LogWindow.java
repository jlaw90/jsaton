package net.cakenet.jsaton.ui.tools;

import net.cakenet.jsaton.util.ImageUtil;

import javax.swing.*;
import java.awt.*;

public class LogWindow extends ToolWindow {
    public final LogPanel log;

    public LogWindow() {
        super("Log");
        log = new LogPanel();
    }

    public Component getContents() {
        return log;
    }

    public Icon getIcon() {
        return ImageUtil.loadIcon("icons/page_white_edit.png");
    }
}