package net.cakenet.jsaton.ui.tools;

import com.sun.awt.AWTUtilities;
import net.cakenet.jsaton.nativedef.WindowManager;
import net.cakenet.jsaton.nativedef.WindowReference;
import net.cakenet.jsaton.util.ColorUtil;
import net.cakenet.jsaton.util.ImageUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;

public class ScreenSelectionOverlay extends JDialog implements MouseListener, MouseMotionListener, KeyListener {
    private final WindowReference root = WindowManager.getDesktop();
    private final Rectangle bounds = root.getBounds();
    private final TooltipOverlay tooltip;
    private CloseMethod closeMethod;
    private boolean showColor;

    public ScreenSelectionOverlay(Window parent, boolean showColor, CloseMethod closeMethod) {
        super(parent);
        setUndecorated(true);
        setModalityType(ModalityType.APPLICATION_MODAL);
        setLayout(new BorderLayout());
        AWTUtilities.setWindowOpaque(this, false);
        final Color background;
        if (System.getProperty("os.name").toLowerCase().startsWith("windows"))
            background = new Color(0, 0, 0, 1);
        else
            background = new Color(0, 0, 0, 0);
        setBackground(background);
        setSize(bounds.width, bounds.height);
        setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
        setLocation(0, 0);
        setAlwaysOnTop(true);
        tooltip = new TooltipOverlay(this, 12, 8);
        Point mousePoint = MouseInfo.getPointerInfo().getLocation();
        tooltip.update(mousePoint.x, mousePoint.y); // Set initial...
        addMouseListener(this);
        addMouseMotionListener(this);
        addKeyListener(this);
        tooltip.addMouseMotionListener(this);
        this.showColor = showColor;
        this.closeMethod = closeMethod;
    }

    public void setVisible(boolean b) {
        if (b) {
            // Show close window popup...
            final JWindow closeHelper = new JWindow(this);
            closeHelper.addMouseMotionListener(this);
            closeHelper.addMouseListener(this);
            closeHelper.setAlwaysOnTop(true);
            ((JComponent) closeHelper.getContentPane()).setBorder(BorderFactory.createLineBorder(Color.BLACK));
            AWTUtilities.setWindowOpaque(closeHelper, false);

            final Color labelColor = Color.YELLOW;
            final JLabel label = new JLabel("Press any key to cancel");
            label.setOpaque(true);
            label.setBackground(labelColor);
            Font f = label.getFont();
            f = f.deriveFont(f.getSize() * 2f);
            label.setFont(f);
            closeHelper.add(label);
            closeHelper.pack();
            closeHelper.setLocation(getWidth() - closeHelper.getWidth(), 0);
            closeHelper.setVisible(true);
            new Thread() {
                long startTime = System.currentTimeMillis();

                public void run() {
                    while (true) {
                        final int STAY_TIME = 2000;
                        final float FADE_TIME = 1000f;
                        int delta = (int) (System.currentTimeMillis() - startTime);
                        if (delta >= STAY_TIME) { // after 3 seconds....
                            int fadeDelta = delta - STAY_TIME;
                            float percentage = Math.max(1f - ((float) fadeDelta / FADE_TIME), 0f);
                            int alpha = (int) (256f * percentage);
                            //*
                            if (AWTUtilities.isTranslucencySupported(AWTUtilities.Translucency.TRANSLUCENT)) {
                                AWTUtilities.setWindowOpacity(closeHelper, percentage);
                            } else/**/ {
                                Color c = new Color(labelColor.getRGB() & (0xffffff | (alpha << 24)), true);
                                label.setBackground(c);
                            }
                            if (alpha == 0)
                                break;
                        }
                        try {
                            Thread.sleep(10);
                        } catch (InterruptedException ie) {
                            /**/
                        }
                    }
                    closeHelper.setVisible(false);
                }
            }.start();
            tooltip.setAlwaysOnTop(true);
            tooltip.setVisible(true);
        }
        super.setVisible(b);
    }

    public void close() {
        tooltip.setVisible(false);
        tooltip.dispose();
        setVisible(false);
        dispose();
    }

    public void mouseClicked(MouseEvent e) {
        if (closeMethod == CloseMethod.CloseOnClick)
            close();
    }

    public void mousePressed(MouseEvent e) {
        if (closeMethod == CloseMethod.CloseOnPress)
            close();
    }

    public void mouseReleased(MouseEvent e) {
        if (closeMethod == CloseMethod.CloseOnRelease)
            close();
    }

    public void mouseEntered(MouseEvent e) {
        updateTooltip(e);
    }

    public void mouseExited(MouseEvent e) {
        updateTooltip(e);
    }

    public void mouseDragged(MouseEvent e) {
        updateTooltip(e);
    }

    public void mouseMoved(MouseEvent e) {
        updateTooltip(e);
    }

    private void updateTooltip(MouseEvent e) {
        tooltip.update(e.getXOnScreen(), e.getYOnScreen());
    }

    public void keyTyped(KeyEvent e) {
    }

    public void keyPressed(KeyEvent e) {
        // Allow to close on any key press...
        close();
    }

    public void keyReleased(KeyEvent e) {
    }

    private class TooltipOverlay extends JWindow {
        private final JLabel label;
        private final int source_dim;
        private final int zoom_mult;
        private final int zoom_dim;
        private final BufferedImage zoomView;
        private long lastUpdate;
        private Thread updateThread;

        public TooltipOverlay(Window owner, int source_dimension, int multiplier) {
            super(owner);
            setLayout(new BorderLayout());
            ((JComponent) getContentPane()).setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
            label = new JLabel();
            label.setOpaque(true);
            add(label);
            source_dim = source_dimension;
            zoom_mult = multiplier;
            zoom_dim = source_dim * multiplier;
            zoomView = new BufferedImage(zoom_dim, zoom_dim, BufferedImage.TYPE_INT_RGB);
            label.setIcon(new ImageIcon(zoomView));
            pack();
            setLocation(-999, -999);
        }

        public void setVisible(boolean b) {
            super.setVisible(b);
            if (b && updateThread == null) {
                updateThread = new Thread() {
                    public void run() {
                        while (isVisible()) {
                            long delta = System.currentTimeMillis() - lastUpdate;
                            if (delta >= 200) {
                                Point mousePoint = MouseInfo.getPointerInfo().getLocation();
                                tooltip.update(mousePoint.x, mousePoint.y); // Set initial...
                            }
                            try {
                                Thread.sleep(10);
                            } catch (InterruptedException ie) {
                                /**/
                            }
                        }
                        updateThread = null;
                    }
                };
                updateThread.setDaemon(true);
                updateThread.setPriority(1);
                updateThread.start();
            }
        }

        public void update(int x, int y) {
            lastUpdate = System.currentTimeMillis();
            int zX = x - source_dim / 2;
            int zY = y - source_dim / 2;
            if (zX < 0)
                zX = 0;
            if (zY < 0)
                zY = 0;
            if (zX + source_dim > bounds.width)
                zX = bounds.width - source_dim;
            if (zY + source_dim > bounds.height)
                zY = bounds.height - source_dim;
            // We have our pixels...
            int[] zoomPix = ImageUtil.zoom(root.capture(zX, zY, source_dim, source_dim),
                    source_dim, source_dim, zoom_mult);

            int rX = x - zX; // Relative x
            int rY = y - zY;
            rX *= zoom_mult;
            rY *= zoom_mult;
            zoomView.setRGB(0, 0, zoom_dim, zoom_dim, zoomPix, 0, zoom_dim);
            Graphics gr = zoomView.getGraphics();
            gr.setColor(ColorUtil.contrasting(zoomPix[rY * zoom_dim + rX]));
            gr.drawRect(rX, rY, zoom_mult, zoom_mult);

            if (showColor) {
                int p = zoomPix[rY * zoom_dim + rX] & 0xffffff; // Don't query for it, just get it from the image
                Color contrasting = ColorUtil.contrasting(p);
                StringBuilder data = new StringBuilder("<html><font color=");
                data.append("#").append(String.format("%06x", (contrasting.getRGB() & 0xffffff))).append(">");
                int r = (p >> 16) & 0xff;
                int g = (p >> 8) & 0xff;
                int b = p & 0xff;
                label.setBackground(new Color(r, g, b));
                data.append("0x").append(String.format("%06x", p)).append("<br/>");
                data.append("r: ").append(r).append("<br/>");
                data.append("g: ").append(g).append("<br/>");
                data.append("b: ").append(b);
                data.append("</html>");
                label.setText(data.toString());
                tooltip.pack();
            }

            int newX = x + 16;
            int width = tooltip.getWidth();
            int height = tooltip.getHeight();
            if (newX + width >= bounds.width)
                newX = x - width - 16;
            int newY = y;
            if (newY + height >= bounds.height)
                newY = bounds.height - height;
            tooltip.setLocation(newX, newY);
            label.repaint();
        }
    }

    public static enum CloseMethod {
        CloseOnPress,
        CloseOnClick,
        CloseOnRelease
    }
}