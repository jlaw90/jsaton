package net.cakenet.jsaton.util;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class ImageUtil {
    public static int[] zoom(int[] source, int sourceWidth, int sourceHeight, int mult) {
        int height = sourceHeight * mult;
        int width = sourceWidth * mult;
        int newLen = width * height;
        int[] n = new int[newLen];

        int off = 0;
        for (int y = 0; y < height; y++) {
            int oOffset = (y / mult) * sourceWidth;
            for (int x = 0; x < width; x++, off++) {
                n[off] = source[oOffset + (x / mult)];
            }
        }
        return n;
    }

    public static BufferedImage loadImage(String resource) {
        try {
            return ImageIO.read(ClassLoader.getSystemResource(resource));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static ImageIcon loadIcon(String resource) {
        return new ImageIcon(loadImage(resource));
    }
}