package net.cakenet.jsaton.util;

import net.cakenet.jsaton.Main;
import net.cakenet.jsaton.config.Configuration;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileManager {
    public static final Path USER_HOME;
    private static final Path APP_DATA;
    private static Path USER_DATA;

    private static Path ensureDirectoryExists(Path path) {
        File f = path.toFile();
        if(!f.exists() && !f.mkdirs())
            throw new RuntimeException("Cannot create directories: " + f.getAbsolutePath());
        return path;
    }

    public static Path getAppDirectory(Path path) {
        return ensureDirectoryExists(APP_DATA.resolve(path));
    }

    public static Path getAppDirectory(String first, String... more) {
        return getAppDirectory(Paths.get(first, more));
    }

    public static File getAppFile(Path path) {
        path = APP_DATA.resolve(path);
        ensureDirectoryExists(path.getParent());
        return path.toFile();
    }

    public static File getAppFile(String first, String... more) {
        return getAppFile(Paths.get(first, more));
    }

    public static Path getUserDirectory(Path path) {
        return ensureDirectoryExists(USER_DATA.resolve(path));
    }

    public static Path getUserDirectory(String first, String... more) {
        return getUserDirectory(Paths.get(first, more));
    }

    public static File getUserFile(Path path) {
        path = USER_DATA.resolve(path);
        ensureDirectoryExists(path.getParent());
        return path.toFile();
    }

    public static File getUserFile(String first, String... more) {
        return getUserFile(Paths.get(first, more));
    }

    public static void setUserDataPath(Path path) {
        Configuration.APP_CONFIG.set("user.data", path.toString());
        USER_DATA = path;
    }

    static {
        USER_HOME = Paths.get(System.getProperty("user.home"));
        APP_DATA = USER_HOME.resolve("." + Main.APP_NAME.toLowerCase());
        File f = APP_DATA.toFile();
        if(!f.exists() && !f.mkdirs())
            throw new RuntimeException("Cannot create application data directory.");
        try {
            Files.setAttribute(APP_DATA, "dos:hidden", true);
        } catch (IOException e) {
            /* Assume not windows... */
        }
    }
}