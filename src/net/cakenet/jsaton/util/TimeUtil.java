package net.cakenet.jsaton.util;

public class TimeUtil {
    public static String timeString(int ms) {
        if(ms < 1000)
            return String.format("%d milliseconds", ms);

        StringBuilder sb = new StringBuilder();
        int elapsed = ms / 1000;
        long sec = (elapsed >= 60 ? elapsed % 60 : elapsed);
        long min = (elapsed = (elapsed / 60)) >= 60 ? elapsed % 60 : elapsed;
        long hrs = (elapsed = (elapsed / 60)) >= 24 ? elapsed % 24 : elapsed;
        if (hrs > 0) {
            if (hrs == 1)
                sb.append("an hour");
            else
                sb.append(hrs).append(" hours");
            if (min > 1)
                sb.append(" and ").append(min).append(" minutes");
        } else if (min > 0) {
            if (min == 1)
                sb.append("a minute");
            else
                sb.append(min).append(" minutes");
            if (sec > 1)
                sb.append(" and ").append(sec).append(" seconds");
        } else {
            if (sec <= 1)
                sb.append("about a second");
            else
                sb.append("about ").append(sec).append(" seconds");
        }
        return sb.toString();
    }
}