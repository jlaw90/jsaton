package net.cakenet.jsaton.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class DigestUtil {
    public static final MessageDigest MD5, SHA;

    public static byte[] MD5(byte[] input) {
        return MD5.digest(input);
    }

    public static byte[] SHA(byte[] input) {
        return SHA.digest(input);
    }

    public static String toString(byte[] hash) {
        StringBuilder sb = new StringBuilder(hash.length * 2);
        for(byte b: hash) {
            String h = Integer.toHexString(b & 0xff);
            if(h.length() < 2)
                sb.append("0");
            sb.append(h);
        }
        return sb.toString();
    }

    static {
        try {
            MD5 = MessageDigest.getInstance("MD5");
            SHA = MessageDigest.getInstance("SHA");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
}