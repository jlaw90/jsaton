package net.cakenet.jsaton.util;

import java.awt.*;
import java.awt.image.BufferedImage;

public class ColorUtil {
    public static int brightness(int c) {
        return (299 * ((c >> 16) & 0xff) + 587 * ((c >> 8) & 0xff) + 114 * (c & 0xff)) / 1000;
    }

    public static Color contrasting(int col) {
        return brightness(col) >= 128 ? Color.BLACK : Color.WHITE;
    }

    public static BufferedImage toImage(int width, int height, int col) {
        int[] pix = new int[width*height];
        for(int i = 0; i < pix.length; i++)
            pix[i] = col;
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        bi.setRGB(0, 0, width, height, pix, 0, width);
        return bi;
    }
}