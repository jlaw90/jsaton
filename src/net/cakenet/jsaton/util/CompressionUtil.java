package net.cakenet.jsaton.util;

import java.io.ByteArrayOutputStream;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class CompressionUtil {
    public static byte[] inflate(byte[] source, boolean nowrap) {
        Inflater inf = new Inflater(nowrap);
        inf.setInput(source);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buf = new byte[100];
        try {
            while (!inf.finished()) {
                int read = inf.inflate(buf);
                baos.write(buf, 0, read);
            }
        } catch (DataFormatException dfe) {
            throw new RuntimeException(dfe);
        }
        return baos.toByteArray();
    }

    public static byte[] deflate(byte[] source, boolean nowrap) {
        Deflater d = new Deflater(Deflater.BEST_COMPRESSION, nowrap);
        d.setInput(source);
        d.finish();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buf = new byte[100];
        while (!d.finished()) {
            int len = d.deflate(buf);
            baos.write(buf, 0, len);
        }
        return baos.toByteArray();
    }
}