package net.cakenet.jsaton.util;

public class InvalidImageDataException extends Exception {
    public InvalidImageDataException() {
        super();
    }

    public InvalidImageDataException(String message) {
        super(message);
    }

    public InvalidImageDataException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidImageDataException(Throwable cause) {
        super(cause);
    }

    protected InvalidImageDataException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}