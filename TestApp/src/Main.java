import javax.swing.*;
import java.awt.*;
import java.awt.event.AWTEventListener;

public class Main implements AWTEventListener {
    private final DefaultListModel<AWTEvent> model = new DefaultListModel<>();
    private final JList<AWTEvent> list = new JList<>(model);
    private Component contentPane;

    public static void main(String[] args) {
        new Main();
    }

    private Main() {
        JFrame jf = new JFrame("Event Window");
        jf.add(new JScrollPane(list));
        contentPane = jf.getContentPane();
        jf.setSize(new Dimension(800, 600));
        jf.setVisible(true);
        long exclude = AWTEvent.CONTAINER_EVENT_MASK | AWTEvent.COMPONENT_EVENT_MASK | AWTEvent.HIERARCHY_EVENT_MASK
                | AWTEvent.HIERARCHY_BOUNDS_EVENT_MASK;
        long events = 0xffffffffffffffffL;
        events &= ~exclude;
        Toolkit.getDefaultToolkit().addAWTEventListener(this, events);
    }

    public void eventDispatched(AWTEvent event) {
        model.addElement(event);
        if(model.size() > 25)
            model.removeElementAt(0);
        //list.ensureIndexIsVisible(model.size() - 1);
    }
}